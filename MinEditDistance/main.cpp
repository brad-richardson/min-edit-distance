#include "MinEdit.h"
#include "DynamicProgrammingMinEdit.h"
#include "LocalAlignment.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <thread>

std::vector<std::vector<int> > distanceMatrix;

void doNothing() {

}

class MinEditThread
{
	std::string str1, str2;
	int i, j;
public:
	explicit MinEditThread(std::string str1, std::string str2, int i, int j)
	{
		this->str1 = str1;
		this->str2 = str2;
		this->i = i;
		this->j = j;
	}
	void operator()() const
	{
		//Fix this to use the MinEdit - NWScore instead so it won't use so much memory
		//DynamicProgrammingMinEdit dpMinEdit(str1, str2);
		MinEdit nwMinEdit(str1, str2);
		distanceMatrix[i][j] = nwMinEdit.getMinEdit();
	}
};

int main() {
	std::vector<std::string> nameArray, strArray;//strArray1, strArray2;
	std::vector<MinEdit> minEdits;
	std::vector<LocalAlignment> localAlignments;

	/*
	std::ifstream input("strings.txt");
	
	while (!input.eof()) {
		std::string str1, str2;
		std::getline(input, str1);
		std::getline(input, str2);
		strArray1.push_back(str1);
		strArray2.push_back(str2);
	}
	*/
	/**/
	std::ifstream input("ebolavirus.mafft.fasta");//("uniprot_sprot.fasta");

	bool readingFirstString = true;
	std::string str = ""; // str1 = "", str2 = "";
	while (!input.eof())
	{
		std::string line;
		std::getline(input, line);
		if (line[0] == '>')
		{
			nameArray.push_back(line);
			// Commit string and then erase to start again
			if (str != "")
				strArray.push_back(str);
			str = "";
		}
		else
		{
			str += line;
		}

		// For reading strings as pairs
		//if (line[0] == '>' && !readingFirstString)
		//{
		//	// Commit strings and then delete
		//	strArray1.push_back(str1);
		//	strArray2.push_back(str2);
		//	str1 = "";
		//	str2 = "";
		//	readingFirstString = true;
		//}
		//else if (line[0] == '>' && readingFirstString && str1 != "")
		//{
		//	// Trash line, start new string
		//	readingFirstString = false;
		//}
		//else if (line[0] != '>' && readingFirstString)
		//{
		//	str1 += line;
		//}
		//else if (!readingFirstString)
		//{
		//	str2 += line;
		//}
		//else
		//{
		//	//Do nothing - very first line
		//}
	}
	//*/

	std::cout << "Done reading file, now computing..." << std::endl;

	//for (int i = 0; i < strArray2.size(); i++) {
	//	localAlignments.push_back(LocalAlignment(strArray1[i], strArray2[i]));
	//	//minEdits.push_back(MinEdit(strArray1[i], strArray2[i]));
	//}

	std::ofstream outputNames("ebolaVirusNamesFile.txt");
	for (std::string s : nameArray) {
		outputNames << s << std::endl;
	}
	outputNames.close();
	
	distanceMatrix.resize(strArray.size());
	for (int i = 0; i < strArray.size(); ++i) {
		distanceMatrix[i].resize(strArray.size());
	}

	int maxThreads = 16;
	std::vector <std::thread> threads;
	for (int i = 0; i < maxThreads; ++i)
		threads.push_back(std::thread(doNothing));
	
	int threadCount = 0;
	std::cout << "Progress: ";
	for (int i = 0; i < strArray.size(); ++i) {
		for (int j = 0; j < strArray.size(); ++j) {
			threads[threadCount].join();

			threads[threadCount] = std::thread(MinEditThread(strArray[i], strArray[j], i, j));
			
			threadCount++;
			if (threadCount == 16)
				threadCount = 0;
		}
		std::cout << i << "/" << strArray.size() << std::endl;
	}
	for (auto i = 0; i < 16; ++i)
		threads[i].join();

	std::ofstream output("ebovResults.txt");

	//for (MinEdit m : minEdits) {
	//	std::string editOutput = m.toString();
	//	//std::cout << editOutput;
	//	output << editOutput;
	//}
	/*for (LocalAlignment m : localAlignments)
	{
		std::string localAlignOutput = m.toString();
		output << localAlignOutput;
	}*/
	for (int i = 0; i < distanceMatrix.size(); ++i) {
		for (int j = 0; j < distanceMatrix[i].size(); ++j) {
			output << distanceMatrix[i][j] << " ";
		}
		output << std::endl;
	}
	output.close();

	std::cout << "FINISHED! Output in results.txt" << std::endl;

	int test;
	std::cin >> test;
	return 0;
}