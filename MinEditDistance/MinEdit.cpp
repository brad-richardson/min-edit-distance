#include "MinEdit.h"

MinEdit::MinEdit(std::string first, std::string second) {
	S = first;
	T = second;

	minEditDist = -1;

	MED();
}

void MinEdit::MED() {
	std::chrono::time_point<std::chrono::system_clock> start, end;
	start = std::chrono::system_clock::now();
	
	//globalAlignmentTraceback(S, T);

	// Minimum memory solve (Hirschberg)
	minEditDist = NWScore(S, T).back();
	//Hirschberg(S, T);

	end = std::chrono::system_clock::now();
	elapsedTime = end - start;
}

// Implemented with this page as reference: http://pages.cs.wisc.edu/~bsettles/ibs08/lectures/02-alignment.pdf
void MinEdit::globalAlignmentTraceback(std::string A, std::string B)
{
	const int MATCH = 1;
	const int OPENGAP = -5;
	const int EXTENDGAP = -2;
	const int MISMATCH = -2;
	//New caches and initial setup
	std::vector<std::vector<int>> M, X, Y;
	M.resize(A.size() + 1);
	X.resize(A.size() + 1);
	Y.resize(A.size() + 1);
	for (int i = 0; i < A.size() + 1; i++)
	{
		M[i].resize(B.size() + 1);
		X[i].resize(B.size() + 1);
		Y[i].resize(B.size() + 1);
	}

	M[0][0] = 0;
	for (int i = 1; i < A.size() + 1; i++){
		M[i][0] = -9999;// std::numeric_limits<int>::min();
	}
	for (int j = 1; j < B.size() + 1; j++){
		M[0][j] = -9999; // std::numeric_limits<int>::min();
	}
	for (int i = 0; i < A.size() + 1; i++){
		X[i][0] = OPENGAP + EXTENDGAP * i;
	}
	for (int j = 1; j < B.size() + 1; j++){
		X[0][j] = -9999;// std::numeric_limits<int>::min();
	}
	for (int j = 0; j < B.size() + 1; j++){
		Y[0][j] = OPENGAP + EXTENDGAP * j;
	}
	for (int i = 1; i < A.size() + 1; i++){
		Y[i][0] = -9999;//std::numeric_limits<int>::min();
	}

	// Iterate through arrays to compute edit scores
	for (int i = 1; i < A.size() + 1; i++){
		for (int j = 1; j < B.size() + 1; j++){
			int matchXandY = M[i - 1][j - 1] +
				(S[i - 1] != T[j - 1] ? MISMATCH : MATCH);

			int insertionX = X[i - 1][j - 1] +
				(S[i - 1] != T[j - 1] ? MISMATCH : MATCH);

			int insertionY = Y[i - 1][j - 1] +
				(S[i - 1] != T[j - 1] ? MISMATCH : MATCH);

			M[i][j] = std::max(matchXandY,
				std::max(insertionX, insertionY));
		}
		for (int j = 1; j < B.size() + 1; j++){
			int openGapX = M[i - 1][j] +
				OPENGAP + EXTENDGAP;

			int extendGapX = X[i - 1][j] + EXTENDGAP;

			X[i][j] = std::max(openGapX, extendGapX);
		}
		for (int j = 1; j < B.size() + 1; j++){
			int openGapY = M[i][j - 1] +
				OPENGAP + EXTENDGAP;

			int extendGapY = Y[i][j - 1] + EXTENDGAP;

			Y[i][j] = std::max(openGapY, extendGapY);
		}

	}

	// Compute traceback with gap penalties
	int currX = A.size(), currY = B.size();
	while (currX > 0 || currY > 0)
	{
		if (currX == 0) {
			for (int i = currY - 1; i >= 0; i--) {
				editsMade.push_back(Edit('-', 'X', B[i]));
			}
			return;
		}
		else if (currY == 0) {
			for (int i = currX - 1; i >= 0; i--) {
				editsMade.push_back(Edit(A[i], 'X', '-'));
			}
			return;
		}

		// Max for [currX-1][currY]
		int maxDeletionX = std::max(M[currX - 1][currY], std::max(X[currX - 1][currY], Y[currX - 1][currY]));

		// Max for [currX][currY - 1]
		int maxDeletionY = std::max(M[currX][currY - 1], std::max(X[currX][currY - 1], Y[currX][currY - 1]));

		// Max for [currX - 1][currY -1]
		int maxSubstitution = std::max(M[currX - 1][currY - 1], std::max(X[currX - 1][currY - 1], Y[currX][currY - 1]));

		int maxValue = std::max(maxDeletionX, std::max(maxDeletionY, maxSubstitution));

		if (maxValue == maxDeletionX)
		{
			// Deletion from A/X
			editsMade.push_back(Edit(A[currX - 1], 'X', '-'));
			currX--;
		}
		else if (maxValue == maxDeletionY)
		{
			// Deletion from B/Y
			editsMade.push_back(Edit('-', 'X', B[currY - 1]));
			currY--;
		}
		else
		{
			// Substitute 
			if (A[currX - 1] == B[currY - 1])
			{
				editsMade.push_back(Edit(A[currX - 1], ' ', B[currY - 1]));
			}
			else
			{
				editsMade.push_back(Edit(A[currX - 1], '$', B[currY - 1]));
			}
			currX--;
			currY--;
		}
	}

}

// Helper function for debugging gap alignment traceback
std::string MinEdit::printVector(std::vector<std::vector<int>> v) {
	std::stringstream ss;
	for (int i = 0; i < v.size(); i++) {
		for (int j = 0; j < v[i].size(); j++) {
			ss << v[i][j] << " ";
		}
		ss << std::endl;
	}
	return ss.str();
}

// Finds min edit distance using only O(2n) memory space (n = size of string)
std::vector<int> MinEdit::NWScore(std::string X, std::string Y) {

	std::vector<std::vector<int>> smCache;
	smCache.resize(2);
	smCache[0].resize(Y.size() + 1);
	// Fill first column with distance to bottom
	for (int i = 0; i < Y.size() + 1; i++) {
		smCache[0][i] = i;
	}
	smCache[1].resize(Y.size() + 1);
	
	// Loop through entire matrix until last column
	for (int i = 1; i < X.size() + 1; i++) { 
		smCache[i % 2][0] = i; // Fill first position
		for (int j = 1; j < Y.size() + 1; j++) {
			int deleteFromX = smCache[(i - 1) % 2][j] + 1;
			int deleteFromY = smCache[i % 2][j - 1] + 1;
			int substitute = smCache[(i - 1) % 2][j - 1] + (X[i - 1] != Y[j - 1] ? 1 : 0);
			smCache[i % 2][j] = std::min(deleteFromX, std::min(deleteFromY, substitute));
		}
	}

	// Returns the final column of the table
	return smCache[X.size() % 2];
}

// New traceback method without needing entire cache
void MinEdit::Hirschberg(std::string X, std::string Y) {
	if (X.size() == 0) {
		for (int i = Y.size() -1; i >= 0; i--) {
			editsMade.push_back(Edit('-', 'X', Y[i]));
		}
	}
	else if (Y.size() == 0) {
		for (int i = X.size() - 1; i >= 0; i--) {
			editsMade.push_back(Edit(X[i], 'X', '-'));
		}
	}
	else if (X.size() == 1 || Y.size() == 1) {
		// Finds the actual edits made for only one column/row
		NeedlemanWunsch(X, Y);
	}
	else {
		int sizeX = X.size();
		int midX = X.size() / 2;
		int sizeY = Y.size();

		// Reassign strings to be able to reverse them
		std::string reverseX = X.substr(midX, sizeX - midX);
		std::reverse(reverseX.begin(), reverseX.end());
		std::string reverseY = Y;
		std::reverse(reverseY.begin(), reverseY.end());

		// Find scores
		std::vector<int> leftScores = NWScore(X.substr(0, midX), Y);
		std::vector<int> rightScores = NWScore(reverseX, reverseY);

		int midY = partitionY(leftScores, rightScores);
		
		// Divide and conquer
		Hirschberg(X.substr(midX, sizeX - midX), Y.substr(midY, sizeY - midY));
		Hirschberg(X.substr(0, midX), Y.substr(0, midY));
	}

}

// Returns the "crossing point" of the two scores
int MinEdit::partitionY(std::vector<int> leftScores, std::vector<int> rightScores) {
	// Reverse right scores and sum all values
	std::reverse(rightScores.begin(), rightScores.end());
	std::vector<int> sum;
	for (int i = 0; i < leftScores.size(); i++) {
		sum.push_back(leftScores[i] + rightScores[i]);
	}
	// Returns index of minimum value
	return std::min_element(sum.begin(), sum.end()) - sum.begin();
}

void MinEdit::NeedlemanWunsch(std::string A, std::string B) {
	std::vector<std::vector<int>> nwCache = fillCacheforNeedlemanWunsch(A, B);
	int i = A.size();
	int j = B.size();
	while (i > 0 || j > 0) {
		//Substitution or equivalent
		if (i > 0 && j > 0 && nwCache[i][j] == nwCache[i - 1][j - 1] + (A[i - 1] != B[j - 1] ? 1 : 0))
		{
			if (A[i - 1] == B[j - 1]) {
				editsMade.push_back(Edit(A[i - 1], ' ', B[j - 1]));
			}
			else {
				editsMade.push_back(Edit(A[i - 1], '$', B[j - 1]));
			}
			i--;
			j--;
		}
		//Deletion from T
		else if (i > 0 && nwCache[i][j] == nwCache[i - 1][j] + 1) // fix this
		{
			editsMade.push_back(Edit(A[i - 1], 'X', '-'));
			i--;
		}
		//Deletion from S
		else if (j > 0 && nwCache[i][j] == nwCache[i][j - 1] + 1) {
			editsMade.push_back(Edit('-', 'X', B[j - 1]));
			j--;
		}
	}
}

//Fills cache for use in traceback for NeedlemanWunsch, which is used in Hirschberg algorithm
std::vector<std::vector<int>> MinEdit::fillCacheforNeedlemanWunsch(std::string A, std::string B) {
	std::vector<std::vector<int>> nwCache;
	// Add one because we need possibility from 0-n
	nwCache.resize(A.size() + 1);
	for (int i = 0; i < A.size() + 1; i++) {
		nwCache[i].resize(B.size() + 1);
		for (int j = 0; j < B.size() + 1; j++) {
			nwCache[i][j] = -1;
		}
	}

	for (int i = 0; i < A.size() + 1; i++) {
		nwCache[i][0] = i;
	}

	for (int j = 0; j < B.size() + 1; j++) {
		nwCache[0][j] = j;
	}

	for (int i = 1; i < A.size() + 1; i++) {
		for (int j = 1; j < B.size() + 1; j++) {
			int deleteFromS = nwCache[i - 1][j] + 1;
			int deleteFromT = nwCache[i][j - 1] + 1;
			int substitute = nwCache[i - 1][j - 1] + (A[i - 1] != B[j - 1] ? 1 : 0);
			nwCache[i][j] = std::min(deleteFromS, std::min(deleteFromT, substitute));
		}
	}
	return nwCache;
}

std::string MinEdit::toString() {
	std::stringstream ss;

	if (minEditDist != -1)
	{
		// First few chars
		char initS[11], initT[11];
		sprintf_s(initS, "%.10s", S.c_str());
		sprintf_s(initT, "%.10s", T.c_str());

		ss << "MED between '" << initS;
		if (S.size() > 10) { //truncated
			ss << "...";
		}
		ss << "' and '" << initT;
		if (T.size() > 10) { //truncated
			ss << "...";
		}
		ss << "': ";
		ss << minEditDist << std::endl;
	}

	ss << "Finished in: " << elapsedTime.count() << " seconds" << std::endl;

	ss << "Changes made: " << std::endl;
	//Characters are added in reverse order, will only show last 75 edits made
	int editsToShow;
	if (CHARSTOPRINT == -1) {
		editsToShow = editsMade.size() - 1;
	}
	else if (editsMade.size() > CHARSTOPRINT) {
		editsToShow = CHARSTOPRINT;
	}
	else {
		editsToShow = editsMade.size() - 1;
	}
	for (int i = editsToShow; i >= 0; i--) {
		ss << editsMade[i].top;
	}
	ss << std::endl;
	for (int i = editsToShow; i >= 0; i--) {
		ss << editsMade[i].change;
	}
	ss << std::endl;
	for (int i = editsToShow; i >= 0; i--) {
		ss << editsMade[i].bot;
	}
	ss << std::endl << std::endl;

	return ss.str();
}