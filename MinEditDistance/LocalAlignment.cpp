#include "LocalAlignment.h"

LocalAlignment::LocalAlignment(std::string first, std::string second)
{
	S = first;
	T = second;

	// Add one because we need possibility from 0-n
	cache.resize(S.size() + 1);
	for (int i = 0; i < S.size() + 1; i++) {
		cache[i].resize(T.size() + 1);
		for (int j = 0; j < T.size() + 1; j++) {
			cache[i][j] = -1;
		}
	}

	std::chrono::time_point<std::chrono::system_clock> start, end;
	start = std::chrono::system_clock::now();

	fillCache();
	findBestLocalAlignment();

	end = std::chrono::system_clock::now();
	elapsedTime = end - start;

	cache.clear();
}

// Implemented with this page as reference: http://pages.cs.wisc.edu/~bsettles/ibs08/lectures/02-alignment.pdf
void LocalAlignment::fillCache()
{
	for (int i = 0; i < S.size() + 1; i++) {
		cache[i][0] = i * -1;
	}

	for (int j = 0; j < T.size() + 1; j++) {
		cache[0][j] = j * -1;
	}

	for (int i = 1; i < S.size() + 1; i++) {
		for (int j = 1; j < T.size() + 1; j++) {
			int deleteFromS = cache[i - 1][j] - 1;
			int deleteFromT = cache[i][j - 1] - 1;
			int substitute = cache[i - 1][j - 1] + (S[i - 1] != T[j - 1] ? -1 : 1);
			cache[i][j] = std::max(deleteFromS, std::max(deleteFromT, std::max(substitute, 0)));
		}
	}
}

void LocalAlignment::findBestLocalAlignment()
{
	int maxX = 0, maxY = 0, maxScore = std::numeric_limits<int>::min();
	// Find max score in array to start traceback
	for (int i = 0; i < cache.size(); i++)
	{
		for (int j = 0; j < cache[i].size(); j++)
		{
			if (cache[i][j] > maxScore) 
			{
				maxX = i;
				maxY = j;
				maxScore = cache[i][j];
			}
		}
	}
	//std::cout << S << " " << T << " " << maxX << "/" << maxY << "/" << maxScore << std::endl;
	//std::cout << printCache();

	// Trace back until the value is zero
	int currX = maxX, currY = maxY;
	//Substitution or equivalent
	while (cache[currX][currY] > 0 && currX > 0 && currY > 0)
	{
		int deletionFromS = cache[currX - 1][currY];
		int deletionFromT = cache[currX][currY - 1];
		int substitution = cache[currX - 1][currY - 1];

		int nextMax = std::max(deletionFromS, std::max(deletionFromT, substitution));

		if (nextMax == deletionFromS)
		{
			editsMade.push_back(Edit(S[currX - 1], 'X', ' '));
			currX--;
		}
		else if (nextMax == deletionFromT)
		{
			editsMade.push_back(Edit(' ', 'X', T[currY] - 1));
			currY--;
		}
		else
		{
			if (S[currX - 1] == T[currY - 1])
			{
				editsMade.push_back(Edit(S[currX - 1], ' ', T[currY - 1]));
			}
			else
			{
				editsMade.push_back(Edit(S[currX - 1], '$', T[currY - 1]));
			}
			currX--;
			currY--;
		}
	}
}

std::string LocalAlignment::toString()
{
	std::stringstream ss;

	ss << "Finished in: " << elapsedTime.count() << " seconds" << std::endl;

	ss << "Best local alignment: " << std::endl;
	//Characters are added in reverse order, will only show last 75 edits made
	int editsToShow;
	if (CHARSTOPRINT == -1) {
		editsToShow = editsMade.size() - 1;
	}
	else if (editsMade.size() > CHARSTOPRINT) {
		editsToShow = CHARSTOPRINT;
	}
	else {
		editsToShow = editsMade.size() - 1;
	}
	for (int i = editsToShow; i >= 0; i--) {
		ss << editsMade[i].top;
	}
	ss << std::endl;
	for (int i = editsToShow; i >= 0; i--) {
		ss << editsMade[i].change;
	}
	ss << std::endl;
	for (int i = editsToShow; i >= 0; i--) {
		ss << editsMade[i].bot;
	}
	ss << std::endl << std::endl;

	return ss.str();
}

std::string LocalAlignment::printCache() {
	std::stringstream ss;
	for (int i = 0; i < cache.size(); i++) {
		for (int j = 0; j < cache[i].size(); j++) {
			ss << cache[i][j] << " ";
		}
		ss << std::endl;
	}
	return ss.str();
}