#ifndef EDIT_H
#define EDIT_H

class Edit {
public:
	char top;
	char change;
	char bot;

	Edit(char top, char change, char bot) :
		top(top), change(change), bot(bot) {}
};

#endif