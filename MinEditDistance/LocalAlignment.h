#ifndef LOCALALIGNMENT_H
#define LOCALALIGNMENT_H

#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <chrono>
#include <iostream>
#include "Edit.h"

class LocalAlignment
{
private:
	const int CHARSTOPRINT = -1; // max chars to print for edits, -1 for all
	const int MATCH = 1;
	const int OPENGAP = -5;
	const int EXTENDGAP = -2;
	const int MISMATCH = -2;

	// S is first string, T is second that we are aligning
	std::string S, T; 
	// M scoring vector
	std::vector<std::vector<int>> cache;
	std::vector<Edit> editsMade;
	std::chrono::duration<double> elapsedTime;

	// Fills cache with the "costs" of different edits to the strings
	void fillCache();

	// Based on scores in M matrices, find best local alignment between two strings
	void findBestLocalAlignment();

	//Debugging
	std::string printCache();

public:
	LocalAlignment(std::string, std::string);

	std::string toString();

};

#endif