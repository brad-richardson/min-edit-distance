#include "DynamicProgrammingMinEdit.h"

DynamicProgrammingMinEdit::DynamicProgrammingMinEdit(std::string first, std::string second) {
	S = first;
	T = second;

	// Add one because we need possibility from 0-n
	cache.resize(S.size() + 1);
	for (int i = 0; i < S.size() + 1; i++) {
		cache[i].resize(T.size() + 1);
		for (int j = 0; j < T.size() + 1; j++) {
			cache[i][j] = -1;
		}
	}
	minEditDist = -1;

	MED();
}

void DynamicProgrammingMinEdit::MED() {
	std::chrono::time_point<std::chrono::system_clock> start, end;
	start = std::chrono::system_clock::now();

	// Dynamic programming / cache (Needleman-Wunsch)
	fillCache();
	minEditDist = cache[S.size()][T.size()];
	traceBack();

	// Recursive solving (Levenshtein)
	//minEditDist = MED(S.size(), T.size());

	end = std::chrono::system_clock::now();
	elapsedTime = end - start;

	cache.clear();
}

// i = size of string S
// j = size of string T
// Returns minimum edit distance between two strings
int DynamicProgrammingMinEdit::MED(int i, int j) { // Indexes into S[i] and T[j]
	// Empty strings
	if (i == 0) {
		return j;
	}
	else if (j == 0) {
		return i;
	}
	// If it already exists in the cache, return that instead
	if (cache[i][j] != -1) {
		return cache[i][j];
	}

	// Divide into smaller problems, find work done
	int deleteFromS = MED(i, j - 1) + 1;
	int deleteFromT = MED(i - 1, j) + 1;
	// Substitute if not equal, else skip and do no work here
	int substitute = MED(i - 1, j - 1) + (S[i - 1] != T[j - 1] ? 1 : 0);

	int min = std::min(deleteFromS, std::min(deleteFromT, substitute));

	// Store into cache so we won't have to recompute every time
	cache[i][j] = min;

	return min;
}

// Fills cache starting from known base cases
void DynamicProgrammingMinEdit::fillCache() {
	for (int i = 0; i < S.size() + 1; i++) {
		cache[i][0] = i;
	}

	for (int j = 0; j < T.size() + 1; j++) {
		cache[0][j] = j;
	}

	for (int i = 1; i < S.size() + 1; i++) {
		for (int j = 1; j < T.size() + 1; j++) {

			int deleteFromS = cache[i - 1][j] + 1;
			int deleteFromT = cache[i][j - 1] + 1;
			int substitute = cache[i - 1][j - 1] + (S[i - 1] != T[j - 1] ? 1 : 0);
			cache[i][j] = std::min(deleteFromS, std::min(deleteFromT, substitute));
		}
	}
}

//find smallest from top value
//based on change made, push new symbol with edits made
void DynamicProgrammingMinEdit::traceBack() {
	int i = S.size();
	int j = T.size();
	while (i > 0 || j > 0) {
		//Substitution or equivalent
		if (i > 0 && j > 0 && cache[i][j] == cache[i - 1][j - 1] + (S[i - 1] != T[j - 1] ? 1 : 0))
		{
			if (S[i - 1] == T[j - 1]) {
				editsMade.push_back(Edit(S[i - 1], '=', T[j - 1]));
			}
			else {
				editsMade.push_back(Edit(S[i - 1], '~', T[j - 1]));
			}
			i--;
			j--;
		}
		//Deletion from T
		else if (i > 0 && cache[i][j] == cache[i - 1][j] + 1)
		{
			editsMade.push_back(Edit(S[i - 1], 'X', ' '));
			i--;
		}
		//Deletion from S
		else if (j > 0 && cache[i][j] == cache[i][j - 1] + 1) {
			editsMade.push_back(Edit(' ', 'X', T[j - 1]));
			j--;
		}
	}
}

std::string DynamicProgrammingMinEdit::printCache() {
	std::stringstream ss;
	for (int i = 0; i < cache.size(); i++) {
		for (int j = 0; j < cache[i].size(); j++) {
			ss << cache[i][j] << " ";
		}
		ss << std::endl;
	}
	return ss.str();
}

std::string DynamicProgrammingMinEdit::toString() {
	std::stringstream ss;

	if (minEditDist != -1)
	{
		// First few chars
		char initS[11], initT[11];
		sprintf_s(initS, "%.10s", S.c_str());
		sprintf_s(initT, "%.10s", T.c_str());

		ss << "MED between '" << initS;
		if (S.size() > 10) { //truncated
			ss << "...";
		}
		ss << "' and '" << initT;
		if (T.size() > 10) { //truncated
			ss << "...";
		}
		ss << "': ";
		ss << minEditDist << std::endl;
	}

	ss << "Finished in: " << elapsedTime.count() << " seconds" << std::endl;

	ss << "Changes made: " << std::endl;
	//Characters are added in reverse order, will only show last 75 edits made
	int editsToShow;
	if (CHARSTOPRINT == -1) {
		editsToShow = editsMade.size() - 1;
	}
	else if (editsMade.size() > CHARSTOPRINT) {
		editsToShow = CHARSTOPRINT;
	}
	else {
		editsToShow = editsMade.size() - 1;
	}
	for (int i = editsToShow; i >= 0; i--) {
		ss << editsMade[i].top;
	}
	ss << std::endl;
	for (int i = editsToShow; i >= 0; i--) {
		ss << editsMade[i].change;
	}
	ss << std::endl;
	for (int i = editsToShow; i >= 0; i--) {
		ss << editsMade[i].bot;
	}
	ss << std::endl << std::endl;

	return ss.str();
}