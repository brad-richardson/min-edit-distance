#ifndef DYNPROGMINEDIT_H
#define DYNPROGMINEDIT_H

#include <algorithm>
#include <string>
#include <iostream>
#include <sstream>
#include <chrono>
#include <vector>
#include <algorithm>
#include "Edit.h"

class DynamicProgrammingMinEdit {
private:
	const int CHARSTOPRINT = -1; // max chars to print for edits, -1 for all
	std::string S;
	std::string T;
	std::vector<Edit> editsMade;
	int minEditDist;
	std::chrono::duration<double> elapsedTime;
	std::vector<std::vector<int>> cache;

	// Find Minimum Edit Distance and store
	void MED();

	// Uses dynamic programming/memoization and cachine
	void fillCache();
	void traceBack();
	std::string printCache();

	// Slow recursive technique (Levenshtein edit distance)
	int MED(int, int);

public:
	DynamicProgrammingMinEdit(std::string, std::string);

	int getMinEdit() { return minEditDist; }

	std::string toString();
};
#endif