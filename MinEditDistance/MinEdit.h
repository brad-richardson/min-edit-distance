#ifndef MINEDIT_H
#define MINEDIT_H

#include <algorithm>
#include <string>
#include <iostream>
#include <sstream>
#include <chrono>
#include <vector>
#include <algorithm>
#include "Edit.h"

class MinEdit {
private:
	const int CHARSTOPRINT = -1; // max chars to print for edits, -1 for all
	std::string S;
	std::string T;
	std::vector<Edit> editsMade;
	int minEditDist;
	std::chrono::duration<double> elapsedTime;

	// Find Minimum Edit Distance and store
	void MED();
	
	//These methods are for finding a traceback with gap penalties
	//Only calculates scores, not actual number of steps to complete
	void globalAlignmentTraceback(std::string, std::string);
	std::string printVector(std::vector<std::vector<int>>);

	// Optimized to only use O(2n) memory space and efficient computation
	std::vector<int> NWScore(std::string, std::string); // Computes MED
	void Hirschberg(std::string, std::string); // New traceback technique since we no longer store cache
	int partitionY(std::vector<int>, std::vector<int>);
	void NeedlemanWunsch(std::string, std::string);
	std::vector<std::vector<int>> fillCacheforNeedlemanWunsch(std::string, std::string);

public:
	MinEdit(std::string, std::string);

	int getMinEdit() { return minEditDist; }

	std::string toString();
};

#endif